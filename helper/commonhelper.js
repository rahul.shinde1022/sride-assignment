const auditlog = require("../db/db");

function commonHelper(){}

commonHelper.prototype.isPrimeNumber = function(number) {
	if (number == 1 || number == 2) {
		return true;
	}
	for (var i=2;i<number;i++) {
		if (number % i == 0) {
			return false;
		}
	}
	return true;
}

commonHelper.prototype.logRequest = async function(logdata){
  const auditLog = new auditlog({
    apires:logdata
  });
  const result = await auditLog.save();
}

module.exports = new commonHelper();
