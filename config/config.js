const config = {
  app : {
    port: process.env.PORT || 3000
  },
  db:{
    host: process.env.HOST || 'localhost',
    port: 27017,
    name: process.env.DBNAME || 'sridelog'
  }
};

module.exports = config;
