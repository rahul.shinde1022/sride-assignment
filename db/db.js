const mongoose = require('mongoose');
const config = require('../config/config');
const connectionStr = `mongodb://${config.db.host}:${config.db.port}/${config.db.name}`;
mongoose.connect(connectionStr,{ useNewUrlParser: true });

const logSchema = new mongoose.Schema({
  date: { type:Date, default: Date.now },
  apires: {type:Object}
});

const AuditLog = mongoose.model('AuditLog', logSchema);

module.exports = AuditLog;
