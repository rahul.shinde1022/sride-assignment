const express = require('express');
const config = require('./config/config');
const primedatecheck = require('./routes/primedatecheck');

const app = express();

app.use('/api/primedatecheck', primedatecheck);

app.listen(config.app.port);
console.log(`Server is running on port ${config.app.port}`);
