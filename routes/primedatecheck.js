const express = require('express');
const router = express.Router();
const fs = require("fs");
const commonHelper = require("../helper/commonhelper");

const today = new Date();
const day = today.getDate();

router.get('/', async (req,res) => {
  if ( commonHelper.isPrimeNumber(day) ) {
        var file = fs.readFileSync('./public/data.json', 'utf8');
        var logdata = JSON.parse(file);
        logdata.value = `Date ${day} is prime`;
        await commonHelper.logRequest(logdata);
        return res.send(logdata);
			} else {
        await commonHelper.logRequest({'value':`Date ${day} is not prime so no date`});
				return res.send( `Date ${day} is not prime so no date` );
      }
});

module.exports = router;
